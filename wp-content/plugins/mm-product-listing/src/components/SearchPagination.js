import React from "react";

export default function SearchPagination({ title, value, handlePagination }) {
  return (
    <button key={value} onClick={handlePagination} value={value}>
      {title}
    </button>
  );
}
